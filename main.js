//Gestion d'un formulaire POST avec Ajax avec jQuery
$(document).ready(function () {

    $("#submit").click( function() {

        //récuperer les données
        let nom = $("#nom").val().trim();
        let prenom = $("#prenom").val().trim();
        let mail = $("#mail").val().trim();
        let msg = $("#msg").val().trim();

        //renvoie le form sous forme d'une string regroupant toutes les infos à la suite
        //let chaine = $('form').serialize() 
        //renvoie le form sous forme d'un tableau avec les infos en clé -> valeur
        let chaine = $('form').serializeArray()

        console.log(chaine);

        //Forcer le remplissage des champs en javascript
        /* if(nom == "" || prenom == ""){
            alert('Veuillez remplir les champs')
            return false;
        } */

        $('#error1').empty();
        $('#error2').empty();
        $('#error3').empty();
        $('#error4').empty();

        /* switch (0) {
            case nom.length :
                $("#error1").html('Merci de remplir ce champ');
            case prenom.length :
                $("#error2").html('Merci de remplir ce champ');
            case mail.length :
                $("#error3").html('Merci de remplir ce champ');
            case msg.length : 
                $("#error4").html('Merci de remplir ce champ');
                break;
            default :
                $.ajax( {
                    type: "POST",
                    url: "submission.php",
                    data: chaine,
                    cache: false,
                    //param1 sera data
                    success: function(param1) {
                        alert(param1);
                        $('#reponse').html(param1);
                    },
                    error: function (xhr, status, error){
                        console.error(xhr);
                    }
                });
                return false;
        }  */


        if(nom.length === 0 && prenom.length === 0){

            //$("#nom").after('<span>Merci de remplir ce champ</span>');
            $("#error1").html('Merci de remplir ce champ');
            $("#error2").html('Merci de remplir ce champ');
            return false;

        }else if (prenom.length === 0){

            //$("#prenom").after('<span>Merci de remplir ce champ</span>');
            $("#error2").html('Merci de remplir ce champ');
            return false;

        }else if(nom.length === 0){
            $("#error1").html('Merci de remplir ce champ');
            return false;

        }else if(mail.length === 0) {

            $("#error3").html('Merci de remplir ce champ');
            return false;

        }else if(msg.length === 0){

            $("#error4").html('Merci de remplir ce champ');
            return false;

        }
        else {
            $.ajax( {
                type: "POST",
                url: "submission.php",
                data: chaine,
                cache: false,
                //param1 sera data
                success: function(param1) {
                    alert(param1);
                    $('#reponse').html(param1);
                },
                error: function (xhr, status, error){
                    console.error(xhr);
                }
            });
            return false;
        }

        /* $.ajax( {
                type: "POST",
                url: "submission.php",
                data: {
                    name: nom,
                    firstName: prenom,
                    email:mail,
                    msg: msg
                },
                cache: false,
                //param1 sera data
                success: function(param1) {
                    alert(param1);
                    $('#reponse').html(param1);
                },
                error: function (xhr, status, error){
                    console.error(xhr);
                }
            });
        }); */
    });

});
